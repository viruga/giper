<?php include "connect.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="windows-1251">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Поиск автомобильных шин</title>
    <link href="https://fonts.googleapis.com/css2?family=Golos+Text:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style/reset.css">
    <link rel="stylesheet" href="style/style.css">
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="header__logo">
                <img src="image/logo-black.png" alt="Giper.by" class="logo">
            </div>
            <div class="header__nav">
                <a href="#">Шины</a>
                <a href="#">Диски</a>
                <a href="#">Админ</a>
                <a href="#">Выбор БД</a>
            </div>
        </div>
    </div>
    <div class="search">
        <div class="container">
            <div class="search__tire">
                <!-- Search line -->
                <?php include "search_line.php"?>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container">
            <div class="item">
                <div class="item__visible">
                    <span>Страна изготовения Cordiant Polar 2 205/55R16 91T тарататв</span>
                    <span>145</span>
                    <span>22</span>
                    <span>122</span>
                    <span>Багория</span>
                </div>
                <div class="item__hide">
                    <span>Страна изготовения Cordiant Polar 2 205/55R16 91T тарататв</span>
                    <span>145</span>
                    <span>22</span>
                    <span>122</span>
                    <span>Багория</span>
                </div>
                <div class="item__hide">   
                    <span>Cordiant Polar 2 205/55R16 91T</span>                    
                    <span>113</span>
                    <sap>12</sap>
                    <sap>100</sap>
                    <sap>Shate-m</sap>
                </div>
            </div>
        </div>
    </div>
           
</body>
</html>
