<?php
function selectOptions($name)
{
    $searchOptions = 'SELECT DISTINCT ' . $name . ' FROM bd_1t WHERE price!=0 ORDER BY ' . $name . ' ASC';
    $resultSearch = mysqli_query($mysqli, $searchOptions);
    while ($row = mysqli_fetch_array($resultSearch)) {
        echo '<option value=' . $row[$name] . '>' . $row[$name] . '</option>';
        
    }

}
?>
<form action="" method="POST" target="_blank">
    <select name="shirina">
        <?php
        selectOptions ('shirina');
        ?>
    </select>
    <span class="search__tire_mobile_hide">/</span>
    <select name="visota">
        <?php
        selectOptions ('visota');
        ?>
    </select>
    <span class="search__tire_mobile_hide">R</span>
    <select name="radius">
        <?php
        selectOptions ('radius');
        ?>
    </select>
    <span class="search__tire_mobile_hide">Сезон:</span>
    <select name="sezon">
        <option value="Зима">Зима</option>
        <option value="Лето">Лето</option>
        <option value="Мотошины">Мотошины</option>
        <option value="Грузовики">Грузовики</option>
    </select>
    <input type="submit" class="submit" value="Поиск" />
</form>